///
/// Defines API to retrieve test data for _this_ module.
/// Relies on the non-module-specific test_data_impl.h
///

#pragma once

// NOTE: must be set via CMake
#ifndef TEST_DATA_PATH
  #error "TEST_DATA_PATH must be defined for unit test data to be found"
#endif

#include "test_data_impl.h"


#define TEST_DATA_ZEROS   "zeros"
#define TEST_DATA_RANDOM  "random"

namespace PLExample_Test {

  struct ExampleTestData : public NumPlTest::TestData {

    ExampleTestData()
      : TestData( TEST_DATA_PATH"/pl_example_test_data.json" )
    {;}

  };

  DEFINE_SINGLETON(ExampleTestData, TEST_DATA_RANDOM)

}
