#include "test_data.h"

#include "pl_example.h"

#include <gtest/gtest.h>

namespace {

  using namespace Numurus;
  using namespace PLExample_Test;

  TEST(TestFunction, ProcessTestDataAsInt)
  {
    PLRoutinePassThru example;

    cv::Mat in( testData<int>() ), out;
    float qualIn = 0.0, qualOut = 0.0;
    auto retval = example.process( &in, &out, qualIn, &qualOut );

    ASSERT_EQ( retval, true );
    ASSERT_EQ( in.size, out.size );
  }

  TEST(TestFunction, ProcessTestDataAsDouble)
  {
    PLRoutinePassThru example;

    cv::Mat in( testData<double>() ), out;
    float qualIn = 0.0, qualOut = 0.0;
    auto retval = example.process( &in, &out, qualIn, &qualOut );

    ASSERT_EQ( retval, true );
    ASSERT_EQ( in.size, out.size );
  }

  TEST(TestFunction, ProcessDataOneAsUnsignedChar)
  {
    PLRoutinePassThru example;

    cv::Mat in( testData<unsigned char>() ), out;
    float qualIn = 0.0, qualOut = 0.0;
    auto retval = example.process( &in, &out, qualIn, &qualOut );

    ASSERT_EQ( retval, true );
    ASSERT_EQ( in.size, out.size );
  }

}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
