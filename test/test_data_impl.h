#pragma once

#include <vector>
#include <string>
#include <sstream>
#include <map>
#include <iostream>

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
namespace pt = boost::property_tree;

#include <opencv2/core.hpp>

namespace NumPlTest {

  //=================================================================

  /// Template-type to OpenCV data type converter functions.
  ///
  /// For example, cvType<float>() return CV_32FC1
  /// while cvType<unsigned char>(3) returns CV_8UC3
  ///
  /// Returns CV_CN_MAX if a valid type cannot be generated.
  template <typename T>
  inline unsigned short cvType( const unsigned char numChannels = 1 )
  { return CV_CN_MAX; }

  /// Template specialization of cvType for int8_t / char.
  template <>
  inline unsigned short cvType<int8_t>( const unsigned char numChannels )
  { return CV_8SC(numChannels); }

  /// Template specialization of cvType for uint8_t / unsigned char.
  template <>
  inline unsigned short cvType<uint8_t>( const unsigned char numChannels )
  { return CV_8UC(numChannels); }

  /// Template specialization of cvType for int16_t / short.
  template <>
  inline unsigned short cvType<int16_t>( const unsigned char numChannels )
  { return CV_16SC(numChannels); }

  /// Template specialization of cvType for uint16_t / unsigned short.
  template <>
  inline unsigned short cvType<uint16_t>( const unsigned char numChannels )
  { return CV_16UC(numChannels); }

  /// Template specialization of cvType for int32_t / int.
  template <>
  inline unsigned short cvType<int32_t>( const unsigned char numChannels )
  { return CV_32SC(numChannels); }

  /// Template specialization of cvType for float
  template <>
  inline unsigned short cvType<float>( const unsigned char numChannels )
  { return CV_32FC(numChannels); }

  //=================================================================


  /// Converts contents of string "str" to type T.  The default
  /// behavior uses the built-in cast operator.
  ///
  /// \todo Error behavior is currently undefined
  template <typename T>
  inline const T stringTo( const std::string &str )
  {
    return T(str);
  }

  /// Template specialization for string to signed char (int8_t)
  template <>
  inline const int8_t stringTo( const std::string &str )
  {
    return std::stoi( str );
  }

  /// Template specialization for string to unsigned char (uint8_t)
  template <>
  inline const uint8_t stringTo( const std::string &str )
  {
    return std::stoi( str );
  }

  /// Template specialization for string to signed short (int16_t)
  template <>
  inline const int16_t stringTo( const std::string &str )
  {
    return std::stoi( str );
  }

  /// Template specialization for string to unsigned short (uint16_t)
  template <>
  inline const uint16_t stringTo( const std::string &str )
  {
    return std::stoi( str );
  }

  /// Template specialization for string to signed int (int32_t)
  template <>
  inline const int32_t stringTo( const std::string &str )
  {
    return std::stoi( str );
  }

  /// Template specialization for string to float
  template <>
  inline const float stringTo( const std::string &str )
  {
    return std::stof( str );
  }

  /// Template specialization for string to double
  template <>
  inline const double stringTo( const std::string &str )
  {
    return std::stod( str );
  }


  //=================================================================

  /// Exception when the "dtype" in the JSON data file is undefined.
  class TestBadDTypeException : public std::exception {
  public:

    TestBadDTypeException() = delete;

    TestBadDTypeException( const std::string &dtype )
      : dtype_(dtype)
    {;}

    virtual const char* what() const throw()
    {
      // TODO.  append data type to returned message?
      return "Unknown data type in JSON file";
    }

  protected:

    std::string dtype_;
  };

  ///
  /// Parses a sequence of values of type T from a string.
  /// String must be the the form "[ v1, v2, v3, ... ]"
  ///
  template <typename T>
  inline std::vector<T> parseDataString( const std::string &str )
  {
    std::vector<T> values;

    // Parse
    std::stringstream ss( str );

    // Skip the opening bracket
    ss.ignore(1);

    while( ss.good() )
    {
        std::string substr;
        getline( ss, substr, ',' );
        values.push_back( stringTo<T>(substr) );
    }

    return values;
  }



  ///
  /// Given a pt::ptree, convert it to a cv::Mat of type T
  ///
  template <typename T>
  inline cv::Mat NEPIJsonToMatTyped( const pt::ptree &mat ) {

    // Assume 4-D array
    // \todo Does not checking if these fields are present/absent
    std::vector<int> sizes;
    sizes.push_back( mat.get<int>("samples") );
    sizes.push_back( mat.get<int>("rows") );
    sizes.push_back( mat.get<int>("cols") );
    sizes.push_back( mat.get<int>("channels") );

    std::vector<T> values;

    // Backwards compatibility with previous format which stored data
    // as a string
    const std::string dataStr( mat.get<std::string>("data") );
    if( dataStr.size() > 0 ) {
      values = parseDataString<T>( dataStr );
    } else {
      // Otherwise, assume it's a list of numbers
      for( auto &entry : mat.get_child("data") ) {

        // According to Propertytree documentation:
        // JSON arrays are mapped to nodes. Each element is a child node
        // with an empty name
        values.push_back( entry.second.get<T>("") );

      }
    }

    // Check there's enough data
    const unsigned int data_count = sizes[0] * sizes[1] * sizes[2] * sizes[3];
    assert( data_count == values.size() );

   const cv::Mat tmp( sizes, cvType<T>(1), values.data() );
   cv::Mat out( tmp.clone() ); // Copy data out of vector

   // Perform cursory validation on the resulting Mat
   assert( out.isContinuous() );
   assert( (unsigned int)out.dims == sizes.size() );
   assert( out.type() == cvType<T>(1) );

   for( unsigned int i = 0; i < sizes.size(); ++i ) {
     assert( out.size[i] == sizes[i] );
   }

   return out;
  }

  ///
  ///
  ///
  inline cv::Mat NEPIJsonToMat( const pt::ptree &mat ) {
    const std::string dtype( mat.get<std::string>("dtype") );

    if( dtype == "int8" ) {
      return NEPIJsonToMatTyped<int8_t>(mat);
    } else if( dtype == "uint8" ) {
      return NEPIJsonToMatTyped<uint8_t>(mat);
    } else if( dtype == "int16" ) {
      return NEPIJsonToMatTyped<int16_t>(mat);
    } else if( dtype == "uint16" ) {
      return NEPIJsonToMatTyped<uint16_t>(mat);
    }else if( dtype == "int32" ) {
      return NEPIJsonToMatTyped<int32_t>(mat);
    } else if( dtype == "float32" ) {
      return NEPIJsonToMatTyped<float>(mat);
    } else if( dtype == "float64" ) {
      return NEPIJsonToMatTyped<double>(mat);
    }else {
      throw TestBadDTypeException( dtype );
    }
  }

  // Loads a JSON file containing a single NEPI struct to a Mat
  inline cv::Mat LoadJsonToMat( const std::string &filename ) {
    pt::ptree tree;
    pt::read_json( filename, tree );
    return NEPIJsonToMat( tree );
  }


  /// TestData loads test data from the specified JSON File as a
  /// map[string] -> cv::Mats.  Provides functions to access test data,
  /// which is cached internally.
  struct TestData {

    typedef std::map< std::string, cv::Mat > MapT;

    /// The Constructor takes a path to the test data JSON file.  At present,
    /// it loads all of the data from the file in the constructor.
    /// Will throw an assert() if it's unable to parse the JSON data.
    ///
    /// \todo Don't use assert, throw an exception instead
    TestData( const std::string &filename )
      : cache_()
    {
      if( fs::is_directory(filename) ) {
        loadDirectory( filename );
      } else if( fs::is_regular_file(filename) ) {
        loadPropertyTree( filename );
      }
    }


    void loadDirectory( const std::string &filename )
    {
      fs::path dir(filename);

      for( auto &entry : fs::directory_iterator(dir) ) {

        const fs::path &file(entry.path());

        if( fs::is_regular_file(file) ) {

          if( file.extension() != ".json" ) continue;

          pt::ptree tree;
          pt::read_json(file.string(),tree);

          fs::path withoutExt = file.stem();
          cache_[withoutExt.string()] = NEPIJsonToMat(tree);
        }
      }

    }


    /// Walks through the top-level dict in the JSON data file, loading each
    /// test data array into the cache.
    void loadPropertyTree( const std::string &filename )
    {
      pt::ptree tree;

      pt::read_json(filename, tree);

      for( const pt::ptree::value_type &v : tree ) {

        // value_type is a std::pair<std::string, pt::ptree>  of (name, child)
        const std::string name( v.first );
        cache_[name] = NEPIJsonToMat( v.second );

      }
    }


    ///
    /// Retrieve test data, keyed by string.   The template paramters sets
    /// the C type of the returned cv::Mat
    ///
    /// \param key    Key for test data to return.  Will assert() if not
    ///               found in the test data.
    ///
    /// \return cv::Mat of type T containing test data
    template <typename T>
    cv::Mat operator()( const std::string &key ) {

      // \TODO:  More robust check for key validity.  throw an exception?
      assert( cache_[key].empty() == false );

      // Make a copy of the cv::Mat of type T,
      // if you don't you'll use a reference to the copy in the cache
      //cv::Mat output; //( cache_[key].size(), cvType<T>( cache_[key].channels() ) );
      cv::Mat output;
      cache_[key].convertTo(output, cvType<T>(1) );

      // Brief validation of the copy
      const auto sz(output.size), cacheSz( cache_[key].size );
      assert( output.dims ==  cache_[key].dims );
      for( unsigned int i = 0; i < output.dims; ++i ) { assert( sz[i] == cacheSz[i] ); }

      return output;
    }

    const cv::Mat &at( const std::string &key ) const {
      return cache_.at(key);
    }

    size_t size() const { return cache_.size(); }

    /// Thin wrapper to allow const iteration
    /// the iterator dereferences to a std::pair< std::string keyname, cv::Mat >
    ///
    MapT::const_iterator begin() const { return cache_.begin(); }
    MapT::const_iterator end() const   { return cache_.end(); }

  protected:

    std::map< std::string, cv::Mat > cache_;

  };

/// Macro to define a singleton test data instance which is available across
/// test cases.
///
///  Not strictly required.
///
///  \todo Perhaps could be done with a GoogleTest fixture instead?
#define DEFINE_SINGLETON(TDCLASS, DEFAULT_KEY) \
  inline TDCLASS &theInstance() { \
    static TDCLASS testDataClass; \
    return testDataClass; \
  } \
  \
  template <typename T> \
  cv::Mat_<T> testData( const std::string &key=DEFAULT_KEY ) { \
    return theInstance().operator()<T>( key ); \
  }

}
