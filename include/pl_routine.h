#ifndef __PL_ROUTINE_H
#define __PL_ROUTINE_H

#include <functional>
#include <map>

#include <opencv2/core/mat.hpp>

namespace Numurus
{

/**
 * @brief      Abstract base class for stand-alone subroutines in a data processing pipeline
 * 
 * 			   Concrete subclasses of this class represent the fundamental building blocks
 * 			   of the Numurus Data Processing Pipeline system. Each implementation is essentially
 * 			   a specific subroutine that may be interlinked with other subroutines (via container
 * 			   Processing Modules, providing the IPC/Network layer).
 * 			   
 * 			   Concrete subclasses must adhere to the public API in this base class, and cannot rely on
 * 			   any other calls from other components of the program into which they are linked. 
 */
class PLRoutine
{
public:
	/**
	 * @brief      Basic status info for a PLRoutine object
	 * 
	 * 			   Concrete subclasses of PLRoutine may want to derive a more
	 * 			   sophisticated status struct from this one.
	 */
	struct Status
	{
		bool initialized_ = false; 		/**< Specifies whether the PLRoutine is initialized */
		bool configured_ = false; 		/**< Specifies whether the PLRoutine is configured */

		uint32_t configure_count_ = 0;	/**< Running non-persistent counter of the number of times that PLRoutine::configure() has been called. */
		uint32_t process_count_ = 0;	/**< Running non-persistent counter of the number of times that PLRoutine::process() has been called. */
	};

	/**
	 * @brief      Basic structure for configuring a PLRoutine.
	 * 
	 * 			   Concrete subclasses of PLRoutine will generally want an accompanying
	 * 			   Config struct derived from this one.
	 */
	struct Config
	{
		std::string name_; 			/**< The name of the routine for logging and identification purposes */
		std::string description_; 	/**< Basic description of the routine available at runtime for help menus, etc */

        std::map<std::string, int> int_params;		/**< Key/Value pairs for integer parameters */
        std::map<std::string, float> float_params; /**< Key/Value pairs for floating-point parameters */
        std::map<std::string, std::string> string_params; /**< Key/Value pairs for string params */
	};

	/**
	 * @brief      Verbosity levels for log messages
	 */
	enum PLMsgVerbosity
	{
		PL_DEBUG, 	/**< Reserverd for debug only messages */
		PL_INFO,	/**< Informational messages */
		PL_WARN,	/**< Warning messages. Program will continue, but issue should be noted */
		PL_ERROR,	/**< Error messages. Program may no longer function properly */
		PL_FATAL	/**< Fatal messages. Program will self terminate */
	};

	/**
	 * @brief      Constructs the object.
	 */
	PLRoutine();
		
	/**
	 * @brief      Destroys the object.
	 */
	virtual ~PLRoutine();

	/**
	 * @brief      Initialize the processing routine and any required resources.
	 * 
	 * 			   Derived classes may override this, but should call back to this base implementation or replicate
	 * 			   its functionality. This function is guaranteed by specification to be called before any others in 
	 * 			   the public API (besides the constructor).
	 *
	 * @return     true if initialization succeeds, false otherwise
	 */
	virtual bool initialize() {status_.initialized_ = true; return true;}

	/**
	 * @brief      Configure the processing routine including bounds checking of parameters.
	 * 
	 * 			   Derived classes may override this, but should call back to this base implementation or replicate
	 * 			   its functionality.
	 * 			   
	 * 			   Containing code can reconfigure at any time by calling this method. Implementations should
	 * 			   update their configure_ struct. and not rely on any cached configuration info in the process() 
	 * 			   implementation.
	 *
	 * @param[in]  config  The configuration struct
	 *
	 * @return     true if successful, false otherwise
	 */
	virtual bool configure(const PLRoutine::Config &config);

	/**
	 * @brief      Pure virtual method to process data
	 * 
	 *             This is the main work function of the class, providing a single data processing subroutine
	 *             for inclusion in the greater processing pipeline. Concrete subclasses must implement this.
	 *
	 *             The function manipulates the input data to fill the output data. No persistent structure
	 *             of the input is required in the output - for example, array dimensions and channel counts
	 *             may change from input to output.
	 *             
	 *             However, there are some input/output conventions that must be respected:
	 *             
	 *             1. All input/output data is three-dimensional, where the third dimension of each array always 
	 *             represents a time sequence. For example, a 1D time sequence (of length k) must be represented 
	 *             with a size [1,1,k], where a single 2D (m x n) image has size [m,n,1].
	 *             
	 *             2. Array Elements are of type uint8_t, int32_t, or float or in OpenCV nomenclature as 
	 *             CV_8UC(n), CV_32SC(n), and CV_32FC(n) respectively, where n is the channel count. The process 
	 *             method must be able to operate on any of these types, though type conversion from input to output 
	 *             is explicitly permitted. 
	 *             
	 *             The output passed in should be constructed by the caller from the default cv::Mat() constructor
	 *             with no implicit size. This function should resize as necessary.
	 *             
	 *             Implementations should make use of readyToProcess() as a first step to ensure that the class has
	 *             been properly initialized and configured prior to data processing.
	 *
	 * @param      data_in      Input data - Always 3 dimensional (with time represented last), single or multichannel.
	 * @param      data_out     Output data - can output any legal form of cv::Mat object
	 * @param      quality_out  Qaulity score - 0.0 to 1.0, subjective score provided to measure quality of output data
	 * 						    independent of quality of input data.
	 *
	 * @return     true if successfully processed, false otherwise (with additional details logged and applied to PLRoutine::status_)
	 */
	virtual bool process(cv::Mat *data_in, cv::Mat *data_out, float quality_in, float *quality_out) = 0;
	
	/**
	 * @brief      Perform any necessary shutdown/resource cleanup
	 * 
	 * 			   Derived classes may override this, but should call back to this base implementation or replicate
	 * 			   its functionality.
	 */
	virtual void shutdown() {status_.initialized_ = false; status_.configured_ = false;}

	/**
	 * @brief      Retrieve a reference to the status.
	 * 
	 *             This may be particularly useful following an error return from one of the other functions.
	 *
	 * @return     Constant reference to this class's status
	 */
	inline const PLRoutine::Status& retrieveStatus() const {return status_;}

	/**
	 * Provides an alias for the logger callback type
	 */
	typedef std::function<void(PLMsgVerbosity, const std::string)> log_callback_t;
	
	/**
	 * @brief      Sets the callback function for the log. 
	 * 
	 *             The default callback simply uses cout and cerr (dependentent of Verbosity)
	 *
	 * @param[in]  log_callback  The log callback
	 */
	void setLogCallback(log_callback_t log_callback);

    /**
     * @brief       Returns the current int params (see ::configure()).
     *
     * @return      Map of param names to integer values.
     */
    std::map<std::string, int> getIntParams();

    /**
     * @brief       Returns the current float params (see ::configure()).
     *
     * @return      Map of param names to float values.
     */
    std::map<std::string, float> getFloatParams();

    /**
     * @brief       Returns the current string params (see ::configure()).
     *
     * @return      Map of param names to string values.
     */
    std::map<std::string, std::string> getStringParams();

protected:
	/**
	 * @brief      Determine if this class is ready to execute its process routine.
	 * 
	 *             At a bare minimum, the class should be initialized and configure. This is the
	 *             default implementation, but subclasses may override with a more specific
	 *             set of requirements.
	 *             
	 *             This method should be called as a first step in the process method
	 *
	 * @return     true if ready, false otherwise
	 */
	virtual inline const bool readyToProcess() const {return ((true == status_.configured_) && (status_.initialized_));}

	PLRoutine::Config config_;	/**< The current configuration, scoped for clarity */
	PLRoutine::Status status_;	/**< The current status, scoped for clarity */
	log_callback_t log;			/**< The log function - invoke this for output */

private:
	/**
	 * @brief      Write the message to stdout/stderr and teriminate if fatal
	 * 
	 * 			   This is the default logger for PLRoutines. Most instatiating
	 * 			   code will want to overwrite this behavior.
	 * 			   
	 * 			   @note {Will throw an exeption if v is FATAL}
	 *
	 * @param[in]  v     { parameter_description }
	 * @param[in]  msg   The message
	 */
	void defaultLog(PLMsgVerbosity v, const std::string msg);
};

} // Namespace Numurus

#endif
