#ifndef __PL_EXAMPLE_H
#define __PL_EXAMPLE_H

#include "pl_routine.h"

namespace Numurus
{

class PLRoutinePassThru  : public PLRoutine
{
public:
    bool initialize();
    bool configure(const PLRoutine::Config &config);
    bool process(cv::Mat *data_in, cv::Mat *data_out, float quality_in, float *quality_out);
};

}

#endif
